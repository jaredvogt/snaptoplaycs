/**
  * this configuration file is specific to someone setting up gameserver - actual numbers should not be included
*/
var from = '+nnnnnnnnnn';  // outgoing SMS messages will coem from this number. For users to hit reply, twilio has to be listening at this number and calling the server that spawned the original SMS
var gameMasterIds = ['+nnnnnnnnnn', '+nnnnnnnnnn']; // a gameMaster can control a game