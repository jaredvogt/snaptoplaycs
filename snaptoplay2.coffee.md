### Primary webserver file for snaptoplay

    Hapi = require('hapi')
    XRegExp = require('xregexp').XRegExp
    test = require('./test')
    newTest = require('./newtest')
    utils = require('./utils')
    fs = require('fs')
    twil = require("./twilioSMS")
    twilioHandler = require('./twilioHandler')
    hapiHandlers = require('./hapiHandlers')

Initialize myStuff and the things I like to do...

    utils.myStuff()
    p = console.log

Set up the server

    # set up all the options for the server
    options =
        # tls:
            # key: fs.readFileSync('/home/ubuntu/.ssh/privatekey.pem', 'utf8')
            # cert: fs.readFileSync('/home/ubuntu/.ssh/certificate.pem', 'utf8') # Turn server into https
        files:
            relativeTo: 'process'  # set the "root" of the server to the directory in which it is launched


    # Listen to log events
    Hapi.Log.on('log', (event) ->
        Hapi.Log.print event  # Send to console

    # Create a server with a host and port
    http = new Hapi.Server('0.0.0.0', 8000, options)

    # Add the routes
    http.addRoute({ method: 'GET', path: '/test', handler: { file: './test.html' } })
    http.addRoute({ method: 'GET', path: '/hello', config: hapiHandlers.hello })  # this is just a test
    http.addRoute({ method: 'GET', path: '/dashboard/{gameId}', config: hapiHandlers.dashboard })  # this will be main console page
    http.addRoute({ method: 'POST', path: '/twilio', config: hapiHandlers.twilio })
    http.addRoute({ method: 'GET', path: '/api/{gameId}', config: hapiHandlers.apiGame })  # all calls to the API will be preceded by api
    http.addRoute({ method: 'GET', path: '/{path*}', handler: { directory: { path: './public/', listing: false } } })  # access to public directory

    # Start the server - we have liftoff
    http.start()


Parse command line options

    pattern = XRegExp('(?<arg> ^\\w+ ) \\s? # grab the first word after js file  \n\
                        (?<more> .? ) # grab anything else ', 'x')
    match = XRegExp.exec(process.argv[2], pattern) # match should contain 2 named captures

Currently this is pretty much for supporting test mode. The code below can migrate into the test.js file at some point to keep hapi nice and clean. output exists only to carry state to test - extraneous if I move code.

    if match.arg == 'test'
        test.promptStart()
        twil.setOutput('to console')
        # twil.output = 'to console'  # come back and use this
        p 'running in test mode - redirect all messages to console/log'
        # test_output =

    else if match.arg == 'newtest'
        newTest.promptStart()
        twil.setOutput('to console')
        # twil.output = 'to console'  # come back and use this
        p 'running in test mode - redirect all messages to console/log'
        # test_output =

    else if match.arg == 'testsms'
        test.promptStart()
        twil.setOutput('to sms')
        # twil.output = 'to console'
        p 'running in test mode - all messages still go out via SMS'
