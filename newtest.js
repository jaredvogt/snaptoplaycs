var twilioHandler = require('./twilioHandler');
var prompt = require('prompt');
var utils = require('./utils');
var output = '';

utils.myStuff();  // initialize myStuff
p = console.log;
gameMasterDefault = '+12065555555';
gameNameDefault = 'wsdemo';

/**
  * setup console log feedback...
*/
prompt.start();

function promptStart() {
  prompt.get(['command'], function (err, result) {
    if (err) { return onErr(err); }
    p('Command-line input received:');
    p('  command: ' + result.command);
    try {
      eval(result.command);
    }
    catch (e) {
      p(e);
    }
    promptStart();
  });
}

function onErr(err) {
  p(err);
  return 1;
}

//var teams = ['+12223334444', '+12223334445', '+12223334446', '+12223334447'];

// messages



/**
  * These are all the various test scenarios... I could put them all in a single object and then construct calls on the fly
*/
function man() {
  p('sim() will launch a full game');
}


function init(gameMaster, gameName) {
  gameMaster = typeof gameMaster !== 'undefined' ? gameMaster : gameMasterDefault;
  gameName = typeof gameName !== 'undefined' ? gameName : gameNameDefault;
  steps = [
  "twilioHandler.twilioProcess('{0}', 'server', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster),
  "twilioHandler.twilioProcess('{0}', 'stats', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster),
  "twilioHandler.twilioProcess('{0}', 'create {1}', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster, gameName),
  "twilioHandler.twilioProcess('{0}', 'stats', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster),
  "twilioHandler.twilioProcess('{0}', 'stats {1}', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster, gameName)
  ];
  for (var i=0; i<steps.length; i++) {
    p('*********step ' + (i+1));
    eval(steps[i]);
  }
}

var teams = [
{'teamId': '+12223334446', 'teamName': 'polyglots', 'timing': [100, 100, 200, 300]},
{'teamId': '+12223334447', 'teamName': 'bog womp', 'timing': [100, 100, 200, 300]},
{'teamId': '+12223334455', 'teamName': 'SNEEZY', 'timing': [100, 100, 200, 300]},
{'teamId': '+12223334466', 'teamName': 'snow wyte', 'timing': [100, 100, 200, 300]},
{'teamId': '+12223334477', 'teamName': 'jack in the box', 'timing': [100, 100, 200, 300]},
{'teamId': '+12223334488', 'teamName': 'lsdkjf', 'timing': [100, 100, 200, 300]},
{'teamId': '+12223334499', 'teamName': 'flight 100', 'timing': [100, 100, 200, 300]},
{'teamId': '+12223334400', 'teamName': 'chaos monkey', 'timing': [100, 100, 200, 300]}
];

function runGame() {
  for (var team in teams) {
    p(team)
    gamePlay(ganeNameDefault, team);
  }
}

function gamePlay(gameName, team) {
  gameName = typeof gameName !== 'undefined' ? gameName : gameNameDefault;
  p(gameName)
  p(team.timing)
  steps = [
  "twilioHandler.twilioProcess('{0}', '{1}', 'fields.SmsSid', 'fields.AccountSid')".format(team.Id, gameName),  // register
  "twilioHandler.twilioProcess('{0}', '{1}', 'fields.SmsSid', 'fields.AccountSid')".format(team.Id, team.Name),  // team name
  "twilioHandler.twilioProcess('{0}', 'false', 'fields.SmsSid', 'fields.AccountSid')".format(team.Id)  // answer to first question
  ];
  for (var i=0; i<steps.length; i++) {
    p('*********step ' + (i+1));
    p(steps[i])
    // eval(steps[i]);
    _i = i;
    _team = team;
    setTimeout(function() {eval(steps[_i]);}, _team.timing[_i]);
  }
}

function start(gameMaster, gameName) {
  gameMaster = typeof gameMaster !== 'undefined' ? gameMaster : gameMasterDefault;
  gameName = typeof gameName !== 'undefined' ? gameName : gameNameDefault;
  steps = [
  "twilioHandler.twilioProcess('{0}', 'start {1}', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster, gameName)
  ];
  for (var i=0; i<steps.length; i++) {
    p('*********step ' + (i+1));
    eval(steps[i]);
  }
}

// one could have a list with the nubmers - and a list with the answers - and create the responses on the fly. And you could have a list of lists, so you could have dif answers for each round
function answer(gameMaster, gameName) {
  gameMaster = typeof gameMaster !== 'undefined' ? gameMaster : gameMasterDefault;
  gameName = typeof gameName !== 'undefined' ? gameName : gameNameDefault;
  steps = [
  "twilioHandler.twilioProcess('+12223334444', 'this is my answer', 'fields.SmsSid', 'fields.AccountSid')",
  "twilioHandler.twilioProcess('+12223334446', 'false', 'fields.SmsSid', 'fields.AccountSid')"
  ];
  for (var i=0; i<steps.length; i++) {
    p('*********step ' + (i+1));
    eval(steps[i]);
  }
}

function stats(gameMaster, gameName) {
  gameMaster = typeof gameMaster !== 'undefined' ? gameMaster : gameMasterDefault;
  gameName = typeof gameName !== 'undefined' ? gameName : gameNameDefault;
  steps = [
  "twilioHandler.twilioProcess('{0}', 'stats', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster),
  "twilioHandler.twilioProcess('{0}', 'stats {1}', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster, gameName)
  ];
  for (var i=0; i<steps.length; i++) {
    p('*********step ' + (i+1));
    eval(steps[i]);
  }
}

function team(teamId, message) {
  steps = [
  "twilioHandler.twilioProcess('" + teamId + "', '" + message + "', 'fields.SmsSid', 'fields.AccountSid')"
  ];
  for (var i=0; i<steps.length; i++) {
    p('*********step ' + (i+1));
    eval(steps[i]);
  }
}

function admin(message) {
  steps = [
  "twilioHandler.twilioProcess('+12066615101'" + message + "', 'fields.SmsSid', 'fields.AccountSid')"
  ];
  for (var i=0; i<steps.length; i++) {
    p('*********step ' + (i+1));
    eval(steps[i]);
  }
}

// Delete the games object
function killGames(gameMaster) {
  gameMaster = typeof gameMaster !== 'undefined' ? gameMaster : gameMasterDefault;
  steps = [
  "twilioHandler.twilioProcess('{0}', 'killgames', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster)
  ];
  for (var i=0; i<steps.length; i++) {
    p('*********step ' + (i+1));
    eval(steps[i]);
  }
}

// Delete a specific game object
function killGame(gameMaster, gameName) {
  gameMaster = typeof gameMaster !== 'undefined' ? gameMaster : gameMasterDefault;
  gameName = typeof gameName !== 'undefined' ? gameName : gameNameDefault;
  steps = [
  "twilioHandler.twilioProcess('{0}', 'killgame {1}', 'fields.SmsSid', 'fields.AccountSid')".format(gameMaster, gameName)
  ];
  for (var i=0; i<steps.length; i++) {
    p('*********step ' + (i+1));
    eval(steps[i]);
  }
}

function sim() {
  init();
  setTimeout(function() {teams();}, 1000);
  setTimeout(function() {start();}, 2000);
  setTimeout(function() {answer();}, 2500);
  setTimeout(function() {killGames();}, (9*60*1000)-1000);
  setTimeout(function() {sim();}, 9*60*1000);
}

function simTwo() {
  init('+13334445555', 'anothergame');
  setTimeout(function() {teams('+13334445555', 'anothergame');}, 1000);
  setTimeout(function() {start('+13334445555', 'anothergame');}, 2000);
  setTimeout(function() {answer('+13334445555', 'anothergame');}, 2500);
}

exports.promptStart = promptStart;


