var utils = require('./utils');

utils.my_stuff();  // initialize my_stuff
p = console.log;

function route(handle, pathname, response, request) {
  p("About to route a request for {0}".format(pathname));
  if (typeof handle[pathname] === 'function') {
    handle[pathname](response, request);
  } else {
    p("No request handler found for {0}".format(pathname));
    response.writeHead(404, {"Content-Type": "text/html"});
    response.write("404 Not found");
    response.end();
  }
}

exports.route = route;
