var _ = require('underscore');
var questions = require("./questions_all").questions;
var utils = require('./utils');
var twil = require("./twilioSMS");

utils.myStuff();  // initialize myStuff
p = console.log;

var messageCallback = function (response) {
  p(response);
  utils.logIt(response);  // log outbound information (this logs the time its sent - it does NOT check to see when twilio sent it - need to build)
};  // this is for sendSms so that it gets response from twil before logging

// this is the callback for all messages
var senderCallback = function (teamId, msg) {
  twil.sendSms(teamId, msg, messageCallback);
};


/**
  * Games Class (holds Games)
*/
function Games(gsmId) {
  this.gsmId = gsmId;  // gsm - game server master id
  this.active = {};
  this.teams = {};
  this.senderCallback = senderCallback;
  this.stage = 'running';
  this.senderCallback(this.gsmId, 'Game server status: {0}'.format(this.stage));

}

// call to create a new game
Games.prototype.create = function(game_id, gmId) {
  this.active[game_id] = new Game(game_id, gmId);
  this.senderCallback(this.gmId, 'Game created: {0}'.format(this.active[game_id].id));
  return this.active[game_id];
};

// call to kill a game (this is not currently functional)
Games.prototype.killGame = function(game_id, gmId) {
  p('received command to kill game');
  this.active.remove(game_id);
  this.senderCallback(this.gmId, 'Game killed');
};

// create object with all teams and associated games (used for quick lookup in twilioHandler - start of a cache)
Games.prototype.get_teamIds = function() {
  // var teams_temp =
  for (var game_id in this.active) {
    for (var teamId in this.active[game_id].teams) {
      this.teams[teamId] = this.active[game_id];
    }
  }
  return this.teams;
};

// stats for all games
Games.prototype.stats = function() {
  p(this.active);
  p(this.stage);
};


/**
  * Game Class (all details of a game, holds Teams)
*/
function Game(id, gmId) {
  this.id = id;
  this.gmId = gmId;
  this.senderCallback = senderCallback;
  this.teams = {};
  this.questions = questions;
  this.qidx = -1;
  this.registerTime = 120*1000;
  this.slop = 15*1000;
  this.rank = 0;  // used by ask - short cut so answer doesn't have to query all teams to find out what rank to assign
  this.timeLeft = 0;
  this.stage = 'register';  // register, play, game over
  this.state = 'in play';  // in play, in slop, game over
  this.timer(this.registerTime);  // this starts the countdown on instantiation...
}

Game.prototype.register = function(teamId) {
  this.teams[teamId] = new Team();
  this.teams[teamId].name = teamId;  // set the name by default to the teamId
  this.senderCallback(teamId, 'Thanks for joining.  Please send us a name for your team (so we can put something more interesting than your phone number on the scoreboard).');
  p('ran register');
};

Game.prototype.setName = function(teamId, name) {
  this.teams[teamId].name = name;
  // There has to be a better way to do this! Get rid of the hardcoding - messages should be a resource pulled in - and messaging should know how to iterate multiple messages per stage
  this.senderCallback(teamId, 'Game on!  This is a SnaptoPlay.  Get ready for some puzzler questions.  Each will be sent to your phone and show on the scoreboard.  Just txt back your answer.');
  that = this;
  setTimeout(function() {
    that.senderCallback(teamId, '60 seconds per "round." 10 points for correct answers, -5 for wrong! Bonus points for answering before other teams. Your first question coming soon…');  // delay this 7 secs
  }, 7 * 1000);
};

Game.prototype.start = function(teamId) {
  // add logic to check and make sure that this gm can start this game
  this.stage = 'play';
  clearInterval(this.timerLoop);
  this.senderCallback(teamId, 'Game started: {0}'.format(this.id));
  this.ask();  // start the questions
};

Game.prototype.stop = function() {
  this.stage = 'game over';
  this.state = 'game over';
  p('stop function called');
};

/// this is a timer that gets triggered by ask and slop
Game.prototype.timer = function(duration) {
  this.durationSeconds = duration / 1000;
  this.timeleft = this.durationSeconds - 1;
  var that = this;
  this.timerLoop = setInterval(function() {
    if (that.durationSeconds === 0) {
      clearInterval(that.timerLoop);
    }
    else {
      that.durationSeconds--;
      p(that.timeLeft);
      that.timeLeft = that.durationSeconds;
    }
  }, 1000);
};

/// Start calls ask() and ask recursively calls itself until it is out of questions
Game.prototype.ask = function() {
  this.state = 'in play';
  this.rank = 1;  // reset rank for each question
  this.qidx++; // increment to the next question - will be 0 the first time...

  if (this.qidx == this.questions.length) {
    this.stop();
    // p('all questions have been asked')
  }
  else {
    this.q = this.questions[this.qidx];
    clearInterval(this.timerLoop);  // clear on that has already been running
    this.timer(this.q.time);  // this starts a count down on the question
    for (var teamId in this.teams) {
      this.senderCallback(teamId, this.q.question);  // this is the call that sends the question out to players
      // "initialize all of the items for a given question"
      this.teams[teamId].score.push(0);
      this.teams[teamId].rank.push(0);
      this.teams[teamId].correct.push(0);
      this.teams[teamId].tries.push([]);
      this.teams[teamId].answers.push(null);
      this.teams[teamId].smsIdOut.push(null);
      this.teams[teamId].smsIdIn.push(null);
    }
    var that = this;
    this.qtimer = setTimeout(function() {that.inSlop();}, that.q.time);  // changing qidx to increment at the end of a round
  }
};

/// Start calls ask() and ask recursively calls itself until it is out of questions
Game.prototype.inSlop = function() {
  this.state = 'in slop';
  clearInterval(this.timerLoop);
  this.timer(this.slop);
  this.calcScore();  // maybe put something in here so the UI can be "caluclating the score" - something for drama... a counter (although that could be entirely in the UI)
  var that = this;
  this.sloptimer = setTimeout(function() {that.ask();}, that.slop);  // changing qidx to increment at the end of a round
};

Game.prototype.answer = function(teamId, answer) {
  if (+new Date() - this.qtime >= this.q.time) {
    this.senderCallback(teamId, 'Oops, too late - wait for next question.');
  }
  else {
    if (this.teams[teamId].answers[this.qidx] === null) {
      this.teams[teamId].answers[this.qidx] = answer;  // assigning answer to current question
      this.teams[teamId].tries[this.qidx].push(1);  // there is no logic here yet for multiple attempts per question
      this.teams[teamId].rank[this.qidx] = this.rank++;  // set the rank
    }
    else {
      this.teams[teamId].answers[this.qidx] = answer;  // assigning answer to current question
      this.teams[teamId].tries[this.qidx].push(1);  // there is no logic here yet for multiple attempts per question
    }

    // evaluate whether answer is correct
    if (this.q.answer.indexOf(answer.toLowerCase()) !== -1) {  // added to lowercase in here
      this.teams[teamId].correct[this.qidx] = 1;
    }
    else {
      this.teams[teamId].correct[this.qidx] = 0;
    }

    // if everyone has answered, kill the timeout and ask the next question
    if (this.rank > _.size(this.teams)) {
      clearTimeout(this.qtimer);
      this.inSlop();
    }
  }
};

// score the round!
Game.prototype.calcScore = function() {
  for (var teamId in this.teams) {
    var oldScore = 0;
    if (this.qidx > 0) {
      oldScore = this.teams[teamId].score[this.qidx - 1];  // not sure what will happen on the first one
    }
    var roundRank = this.teams[teamId].rank[this.qidx];
    p("roundrank: " + roundRank);
    p("score: " + this.teams[teamId].score[this.qidx]);
    var roundPotential = _.size(this.teams) * (this.qidx + 1);
    p("total points this round: {0}".format(roundPotential));

    // get whether correct
    if (this.teams[teamId].correct[this.qidx] === 0 || roundRank === 0) {  // they answered wrong or didn't answer...
      this.teams[teamId].score[this.qidx] = oldScore;
    }
    else {
      // calculate
      p(this.teams[teamId].score[this.qidx] = (roundPotential - roundRank + 1) + oldScore);  //
    }
  }
};

// basic stats that get echoed back to console
Game.prototype.stats = function() {
  p(this.teams);
  p(this.rank);
  for (var i=0; i<this.teams.length; i++) {
    p(this.teams.score);
  }
};

// package up everything as a json bundle for use by browser to show current stats
Game.prototype.apiStats = function() {
  p(this.teams);
  p(this.rank);
  return this.teams;
};

// package up everything as a json bundle for use by browser to show current stats for current round - to be used by game console
Game.prototype.apiStatsSnapshot = function() {
  var teamInfo = {};
  var roundInfo = {};
  var gameInfo = {};
  var serverInfo = {};
  for (var teamId in this.teams) {
    p('here is all the data for the round for team: {0}'.format(teamId));
    var score = this.teams[teamId].score[this.qidx];
    if (this.qidx > 0 && this.state === 'in play') {  // after the first round, if in play, show score from previous round - when we move to slop, it shows new score - and then flips back to previous round in play
      score = this.teams[teamId].score[this.qidx - 1];
    }
    var snapshot = {
      name: this.teams[teamId].name,
      score: score,
      rank: this.teams[teamId].rank[this.qidx],
      correct: this.teams[teamId].correct[this.qidx],
      tries: this.teams[teamId].tries[this.qidx] || null,
      answer: this.teams[teamId].answers[this.qidx] || null
    };
    teamInfo[teamId] = snapshot;
    roundInfo['round'] = this.qidx + 1;  // this is a little confusing, but the round numbers start at 0 - but prob want to display + 1
    roundInfo['question'] = this.q.question;
    roundInfo['timeLeft'] = this.timeLeft;
    gameInfo['state'] = this.state;
    gameInfo['stage'] = this.stage;
  }
  return {'teams': teamInfo, 'round': roundInfo, 'game': gameInfo, 'server': serverInfo};
};


/**
  * Create the Team class
*/
function Team() {
    this.name=null;  // this can be set from another text
    this.score=[];  // this is cummulative...
    this.rank=[];  // this is the rank by question
    this.correct=[];  // this is either 1 or 0 (this could be derrived in real time)
    this.tries=[];  // for multi answer questions, this is number of tries
    this.answers=[];  // this is the answers
    this.smsIdOut = [];  // this shld have all ids for outbound sms messages [{id: [accepted, sent]}]
    this.smsIdIn = [];  //
}

exports.Game = Game;
exports.Games = Games;

