#!/bin/bash

# installing redis
cd $HOME
curl -C - -O http://download.redis.io/redis-stable.tar.gz
tar xzf redis-stable.tar.gz
cd redis-stable
make
mv src/redis-server /bin/.
mv src/redis-cli /bin/.
